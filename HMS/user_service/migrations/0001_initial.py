# Generated by Django 3.0.3 on 2020-05-23 10:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContactRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.IntegerField(blank=True)),
                ('email', models.CharField(max_length=100, null=True)),
                ('phone', models.CharField(max_length=13, null=True)),
                ('details', models.TextField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
            options={
                'db_table': 'contact_request',
            },
        ),
        migrations.CreateModel(
            name='Login',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=255, unique=True)),
                ('password', models.CharField(max_length=255)),
                ('role', models.CharField(default='user', max_length=64)),
                ('active', models.BooleanField(default=False)),
                ('is_blocked', models.BooleanField(default=False)),
                ('last_login', models.DateTimeField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'db_table': 'login',
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(default='', max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('profile_photo', models.FileField(default='images/no_image1.png', upload_to='images/profile_photos')),
                ('gender', models.CharField(max_length=32)),
                ('email', models.EmailField(max_length=255, unique=True)),
                ('phone', models.CharField(max_length=32)),
                ('designation', models.CharField(max_length=128)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('login', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='login_data', to='user_service.Login', unique=True)),
            ],
            options={
                'db_table': 'hms_users',
            },
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('auth_ticket', models.CharField(default='not defined', max_length=1024)),
                ('login', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='user_service.Login')),
            ],
            options={
                'db_table': 'tokens',
            },
        ),
        migrations.CreateModel(
            name='LoginHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('login', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user_service.Login')),
            ],
            options={
                'db_table': 'login_history',
            },
        ),
    ]
